package exampleplugin;

import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.apache.felix.dm.annotation.api.Component;

@Component(provides=Plugin.class)
public class HelloPlugin extends AbstractBasePlugin{

	@Command 
	public void hello() {
		System.out.println("Hello from plugin");
	}
	
	public String getName() {
		return "example";
	}

	public boolean isInstalled() {
		// TODO Auto-generated method stub
		return false;
	}

}
